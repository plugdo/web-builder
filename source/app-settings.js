var config = {
    
}

const settings = {
    projectPath: {
        custom: false,
        location: ""
    },
    security: {
        rateLimit: {
            active: true,
            period: 1,
            limit: 100
        },
        header: {
            active: true
        },
        origin: {
            active: true,
            domains: ['http://localhost']
        }
    },
    middlewares: [] // add functions like function (req, res, next) used by express
}

var data = {
    config: config,
    settings: settings
}

module.exports = data;