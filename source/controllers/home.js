let options = {
    name: "home",
    action: "index",
    path: ""
};

mvc.controller(options, "demoService", function (req, demoService, res) {
    res({
        message: demoService.getMessage()
    });
});