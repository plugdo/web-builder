const node = require("plugdo-node").node();
const path = require("path");
var cookieParser = require('cookie-parser');

const port = process.env.PORT === undefined ? 80 : process.env.PORT;

const _const = require("./source/app-settings");
global.config = _const.config;
global.settings = _const.settings;

mvc.middlewareBefore.push(cookieParser());

if(global.settings.middlewares.length > 0) {
    global.settings.middlewares.forEach((callback) => {
        mvc.middlewareBefore.push(callback);
    });
}

node.start(port, path.resolve(__dirname));