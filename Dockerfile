FROM keymetrics/pm2:latest-alpine
LABEL maintainer="marco@plugdo.com"

RUN apk add --no-cache git bash openssh

WORKDIR /src
COPY . .

RUN npm install --production

CMD ["sh", "-c", "pm2-runtime start pm2.json"]